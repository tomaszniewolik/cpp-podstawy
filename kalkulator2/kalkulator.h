#ifndef KALKULATOR_H_INCLUDED
#define KALKULATOR_H_INCLUDED

double dodawanie(double a, double b);
double odejmowanie(double a, double b);
double mnozenie(double a, double b);
double dzielenie(double a, double b);
void wczytajLiczby(double &a, double &b);

#endif // KALKULATOR_H_INCLUDED
