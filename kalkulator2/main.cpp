#include <iostream>
#include "kalkulator.h"

using namespace std;

int menu();
double potega(double podstawa, int  wykladnik);

int main()  
{
    int opcjaMenu = 0;
    double l,ll;

    while (true) {
        opcjaMenu = menu();
        if (opcjaMenu == 0) {
            break;
        }
        switch(opcjaMenu) {
        case 1:
               wczytajLiczby(l, ll);
            cout<< dodawanie(l, ll) <<endl;
            break;
        case 2:
               wczytajLiczby(l, ll);
            cout<< odejmowanie(l, ll) <<endl;
            break;
        case 3:
               wczytajLiczby(l, ll);
            cout<< mnozenie(l, ll) <<endl;
            break;
        case 4:
            wczytajLiczby(l, ll);
            cout<< dzielenie(l, ll) <<endl;
            break;
        case 5:
            wczytajLiczby(l, ll);
            cout<< potega(l, ll) <<endl;
            break;
        default:
            cout << "wybrales bledna opcje" << endl;
        }
    }
    cout << "Do zobaczenia!" << endl;
    return 0;
}

double potega(double podstawa, int  wykladnik) {
    if (wykladnik==0) {
        return 1.0;
    } else if (wykladnik==1) {
        return podstawa;
    } else {
        return podstawa *potega(podstawa, wykladnik-1);
    }
}


int menu() {
    int wybor;
    cout << "**** MENU ****" << endl << "1 - dodawanie" << endl << "2 - odejmowanie" << endl<< "3 - mnozenie"
             <<endl<<"4 - dzielenie" <<endl<<"5 - potegowanie" <<endl<< "0 - koniec" << endl << "Wybieram: ";
    cin >> wybor;
    return wybor;
}

