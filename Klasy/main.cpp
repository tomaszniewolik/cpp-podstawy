#include <iostream>
#include <fstream>

using namespace std;

class TSamochod {
            string model;
            string marka;
public:
            static int licznik;
            int rocznik;

            TSamochod() {
                cout << "Konstruktor bezparametrowy.\n";
                model = "Domyślny";
                marka = "Domyślna";
                rocznik = 2022;
                licznik++;
            }

            TSamochod(string marka, string model, int rocznik) {
                cout << "Konstruktor trój parametrowy.\n";
                this->model = model;
                this->marka = marka;
                this->rocznik = rocznik;
                TSamochod::licznik++;
            }

            TSamochod(const TSamochod &copy) {
                cout << "Konstruktor kopiujący.\n";
                this->model = copy.model;
                this->marka = copy.marka;
                this->rocznik = copy.rocznik;
                TSamochod::licznik++;
            }

            TSamochod operator+(const TSamochod& s) {
                cout << "Operator dodawania.\n";
                TSamochod sam;
                sam.marka = this->marka + " "+ s.marka;
                sam.model = this->model + " "+ s.model;
                sam.rocznik = (this->rocznik>s.rocznik?this->rocznik:s.rocznik);
                return sam;
            }

            TSamochod operator-(const TSamochod& s) {
                cout << "Operator odejmowania.\n";
                TSamochod sam;
                sam.marka = this->marka + " minus";
                sam.model = s.model + " minus";
                sam.rocznik = (this->rocznik<s.rocznik?this->rocznik:s.rocznik);
                return sam;
            }

            void drukuj() {
                cout << "Samochód " << this->marka << " " << this->model << " z rocznika " << rocznik << ". \n";
                info();
            }

            static void info() {
                cout << "To już " << TSamochod::licznik << " egzemplarz klasy. \n";
            }
};

int TSamochod::licznik = 0;

int main()
{
    cout << "Witaj w świecie klas!" << endl;
    TSamochod::info();
    TSamochod car1;
    car1.drukuj();
    TSamochod car2("Citroen", "C3", 1998);
    car2.drukuj();
//    car1.marka = "Fiat";
    TSamochod car3 = car2;
    car2.rocznik = 2005;
    car2.drukuj();
    car3.drukuj();
    TSamochod car4("Fiat", "Panda", 1998);
    car4.drukuj();
    TSamochod car5 = car2 + car4;
    car5.drukuj();
    TSamochod car6 = car2 - car4;
    car6.drukuj();

    fstream myFile("plik.txt");
    if (myFile.is_open()) {
        myFile << "Files can be tricky, but it is fun enough!\n";
        myFile << "Last line.\n";
        myFile.close();
    }

    fstream myFileRead("plik.txt");
    if (myFileRead.is_open()) {
        string line;
        int i=1;
        while ( getline (myFileRead, line, ' ') )
        {
            cout << i++ << ". " << line << '\n';
        }
        myFileRead.close();
    }

    return 0;
}
