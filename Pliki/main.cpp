#include <iostream>
#include <fstream>


using namespace std;

class TKsiazka {
    public:
    string autor;
    string tytul;
    string ISBN;

    TKsiazka(string a, string t, string isbn) {
        this->autor = a;
        this->tytul = t;
        this->ISBN = isbn;
    }
    string toString() {
        string txt = autor + ";" + tytul +";" + ISBN + ";";
        return txt;
    }
};

class TLista {
public:
    TKsiazka* ksiazka;
    TLista* wsk;
    TLista() {
        ksiazka = NULL;
        wsk = NULL;
    }

    void dodajDoListy(TKsiazka* wskKsiazka) {
        if (!ksiazka) {
            ksiazka = wskKsiazka;
            return;
        }
        if (this->wsk) {
            TLista* tmp = wsk;
            while(tmp->wsk) {
                tmp = tmp->wsk;
            }
            TLista* nowy = new TLista();
            tmp->wsk = nowy;
            nowy->ksiazka = wskKsiazka;
        } else {
            TLista* nowy = new TLista();
            this->wsk = nowy;
            nowy->ksiazka = wskKsiazka;
        }
    }
};

TKsiazka* t(string tekst, string przerwa = " ")
{
  int start = 0;
  int koniec = tekst.find(przerwa);
  string tablica[3];
  int licznik = 0;
  while(koniec != -1)
  {
      tablica[licznik] = tekst.substr(start, koniec - start) ;
      licznik++;
      start = koniec + przerwa.size();
      koniec = tekst.find(przerwa, start);
  }
  tablica[licznik] = tekst.substr(start, koniec - start) ;
  return new TKsiazka(tablica[0], tablica[1], tablica[2]);
}



const string  DELIMITER= ";";

int main()
{
    cout << "Zaczynam operacje na plikach!\n";

    TLista* head = new TLista();
    fstream plik3;
    plik3.open( "biblioteka.txt", ios::in );
    TKsiazka* wskaznik;
    if( plik3.good() )
    {
        string linia;
        cout << "Zawartosc pliku:" << endl;
        while( !plik3.eof() )
        {
            getline( plik3, linia );
            if (linia.length()>0) {
                cout << linia << endl;
                wskaznik = t(linia, DELIMITER);
                head->dodajDoListy(wskaznik);
                cout << wskaznik->toString() << endl;
                cout << "**************************************" << endl;
            }
        }
        plik3.close();
    }
    else cout << "Error! Nie udalo otworzyc sie pliku!" << endl;
    cout << "****************** LISTA ********************" << endl;

    TLista* temp = head;
    int licznik = 1;
    if (temp) {
        cout << "Lista " << licznik << " " << temp->ksiazka->toString() << endl;
        while (temp->wsk) {
            temp = temp->wsk;
            licznik++;
            cout << "Lista " << licznik << " " << temp->ksiazka->toString() << endl;
        }
    }

    return 0;
}
