#include <iostream>
#include "include/TWektor.h"

using namespace std;

int main()
{
    cout << "Witaj klaso!" << endl;
    TPunkt punkt;
    punkt.informacja();
    punkt.opis();
    punkt.ustaw(1,2);
    punkt.opis();
    punkt.info = "informacja";
 //   punkt.prywatna();

    TPunkt p2{11,12};
    p2.opis();

    TPunkt p3 = p2;
    p3.opis();
    cout << punkt.licznik << endl;
    cout << p2.licznik << endl;
    cout << p3.licznik << endl;
    punkt.licznik = 10;
    cout << punkt.licznik << endl;
    cout << p2.licznik << endl;
    cout << p3.licznik << endl;
    TPunkt::licznik = 20;
    cout << punkt.licznik << endl;
    cout << p2.licznik << endl;
    cout << p3.licznik << endl;

    cout << "******************************************\n";

    TWektor wektor;
    wektor.poczatek.x = 0;
    wektor.poczatek.y = 0;
    wektor.koniec.x = 123;
    wektor.koniec.y = 321;

    cout << wektor.dlugosc() << endl;
    cout << "******************************************\n";

    TPunkt p4 = punkt + p2;
    p4.opis();
    return 0;
}
