#include <iostream>
#ifndef TWEKTOR_H
#define TWEKTOR_H

using namespace std;


class TPunkt {
public:
    int x;
    int y;
    string info;
    static int licznik;

    void opis();

    void ustaw(int x, int y);

    TPunkt operator+(const TPunkt &punkt) {
        cout << "Operator dodawania.\n";
        TPunkt wynik;
        wynik.x = this->x + punkt.x;
        wynik.y = this->y + punkt.y;
        return wynik;
    }

    void informacja() {
        cout << "Jestem obiektem klasy TPunkt\n";
        prywatna();
    }

    TPunkt() {
        x = 0;
        y = 0;
        cout << "konstruktor domyślny, bezparametrowy\n";
    }

    TPunkt(int x, int y) {
        this->x = x;
        this->y = y;
        cout << "konstruktor dwuargumentowy\n";
    }

    TPunkt(const TPunkt &oryginal) {
        this->x = oryginal.x;
        this->y = oryginal.y;
        this->info = oryginal.info;
        cout << "konstruktor kopiujący\n";
    }

    ~TPunkt() {
        cout << "destruktor działa, czyszczenie tego co stworzyła obiekt klasy\n";
    }

private:


    void prywatna() {
        cout << "nie widzisz mnie\n";
    }
};

class TWektor
{
    public:
        TPunkt poczatek;
        TPunkt koniec;

        double dlugosc();

        TWektor();

    protected:

    private:
};

#endif // TWEKTOR_H
