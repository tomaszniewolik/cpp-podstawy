#include <iostream>

using namespace std;

int menu();

double dodawanie(double a, double b)
{
    return a + b;
}

int main()
{
    double a = 1.1;
    double b = 2.2;

    cout << "a=" << a << "   b=" << b << endl;
    cout << "dodawanie: " << dodawanie(a,b) << endl;
    cout << "a=" << a << "   b=" << b << endl;

    return 0;
    int opcjaMenu = 0;

    while (true) {
        opcjaMenu = menu();
        if (opcjaMenu == 0) {
            break;
        }
        switch(opcjaMenu) {
        case 1:
            cout << "dodawanie" << endl;
            break;
        case 2:
            cout << "odejmowanie" << endl;
            break;
        default:
            cout << "wybrales bledna opcje" << endl;
        }
    }
    cout << "Do zobaczenia!" << endl;
    return 0;
}

int menu() {
    int wybor;
    cout << "*** MENU ***" << endl << "1 - dodawanie" << endl << "2 - odejmowanie" << endl << "0 - koniec" << endl << "Wybieram: ";
    cin >> wybor;
    return wybor;
}

